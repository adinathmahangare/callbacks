const fs = require('fs');

const { readFile, convertToUppercaseAndWrite, processAndWriteFile, sortFileContent, deleteFilesInList } = require('./../problem2.cjs');

const inputFilePath = './lipsum_1.txt';
const upperCaseFilePath = './lipsum_uppercase.txt';
const processedFilePath = './lipsum_processed.txt';
const sortedFilePath = './lipsum_sorted.txt';
const filenamesFilePath = './filenames.txt';


readFile(inputFilePath, (err, data) => {
    if (err) {
        console.error('Error reading file:', err);
        return;
    } else {
        convertToUppercaseAndWrite(data, upperCaseFilePath, (err, newFilePath1) => {
            if (err) {
                console.error('Error converting to uppercase and writing:', err);
                return;
            } else {
                    fs.appendFile(filenamesFilePath, `${newFilePath1}\n`, (err) => {
                        if (err) {
                            console.error('Error appending to filenames.txt:', err);
                            return;
                        } else {
                            processAndWriteFile(newFilePath1, processedFilePath, (err, newFilePath2) => {
                                if (err) {
                                    console.error('Error processing and writing file:', err);
                                    return;
                                } else {
                                    fs.appendFile(filenamesFilePath, `${newFilePath2}\n`, (err) => {
                                        if (err) {
                                            console.error('Error appending to filenames.txt:', err);
                                            return;
                                        } else {
                                            sortFileContent(newFilePath2, sortedFilePath, (err, newFilePath3) => {
                                            if (err) {
                                                console.error('Error sorting file content:', err);
                                                return;
                                            } else {
                                                fs.appendFile(filenamesFilePath, `${newFilePath3}\n`, (err) => {
                                                    if (err) {
                                                        console.error('Error appending to filenames.txt:', err);
                                                        return;
                                                    } else {
                                                        deleteFilesInList((err) => {
                                                            if (err) {
                                                                console.error('Error deleting files:', err);
                                                                return;
                                                            }
                                                            console.log('All operations completed successfully.');
                                                        });
                                                    }
                                                });
                                            }                                               
                                        });
                                    }
                                });        
                            }
                        });         
                    }                            
                });
            }
        });                
    }        
});


