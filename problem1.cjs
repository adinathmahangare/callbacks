const fs = require('fs');

function createAndDeleteRandomJsonFiles(directoryName, callback) {
    fs.mkdir(directoryName, { recursive: true }, (err) => {
        if (err) {
            console.error(err);
            return; 
        }
        console.log("folder created ")
    }); 
    
    for (let index = 0; index < 4; index++) {
        fs.writeFile(`./${directoryName}/file${index}.json`, "Hi", (err) => {
            if (err) {
                console.error(err);
                return;
            }
                console.log(`'file created ${index}`)

        });
    }
    callback(directoryName);      
}

function deleteFiles(directoryName) {
    let filePath;

    for (let index = 0; index< 4; index++) {

        filePath = `./${directoryName}/file${index}.json`;

        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(`Error deleting file: ${err.message}`);
            } else {
                console.log(`Deleted file${index}`);
                return;
            }
        });
    }        
}

module.exports = {
    createAndDeleteRandomJsonFiles,
    deleteFiles    
}