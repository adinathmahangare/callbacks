const fs = require('fs');

function readFile(filePath, callback) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            callback(err);
            return;
        }
        callback(null, data);
    })
}

function convertToUppercaseAndWrite(data, newFilePath, callback) {
    const upperCaseData = data.toUpperCase();
    fs.writeFile(newFilePath, upperCaseData, (err) => {
        if (err) {
            callback(err);
            return;
        }

        callback(null, newFilePath);
    });
}

function processAndWriteFile(filePath, newFilePath, callback) {

    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            callback(err);
            return;
        }

        const lowerCaseData = data.toLowerCase();
        const sentences = lowerCaseData.split(/[.!?]/);
        const processedData = sentences.join('\n');
        fs.writeFile(newFilePath, processedData, (err) => {
            if (err) {
                callback(err);
                return;
            } 
            callback(null, newFilePath);
        })
        
    })
}

function sortFileContent(filePath, newFilePath, callback) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            callback(err);
            return;
        }
        const sortedData = data.split('\n').sort().join('\n');
        fs.writeFile(newFilePath, sortedData, (err) => {
            if (err) {
                callback(err);
                return;
            }
            callback(null, newFilePath);
        });
    });
}

function deleteFilesInList(callback) {
    fs.readFile('filenames.txt', 'utf-8', (err, data) => {
        if (err) {
            callback(err);
            return;
        }
        const filesToDelete = data.trim().split('\n');
        let completedDeletions = 0;
        let deletionErrors = [];

        filesToDelete.forEach((file) => {
            fs.unlink(file.trim(), (err) => {
                if (err) {
                    deletionErrors.push(`Error deleting file ${file}: ${err.message}`);
                } else {
                    console.log(`Deleted file: ${file}`);
                }
                completedDeletions++;

                if (completedDeletions === filesToDelete.length) {
                    if (deletionErrors.length > 0) {
                        callback(deletionErrors.join('\n'));
                    } else {
                        callback(null);
                    }
                }
            });
        });
    });
}

module.exports = {
    readFile, convertToUppercaseAndWrite, processAndWriteFile, deleteFilesInList, sortFileContent
};